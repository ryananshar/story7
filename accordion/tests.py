from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *

# Create your tests here.
class uniTest(TestCase):
	def test_existing_url(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_template(self):
		response = Client().get('')
		self.assertTemplateUsed(response, 'index.html')

	def test_landing_page_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_button(self):
		response = Client().get("")
		content = response.content.decode("utf8")
		self.assertIn("<button",content)